# Zaaksysteem Builder
This repository hosts a Dockerfile which is used to build a 'zaaksysteem builder' image.

This repository is part of the [zaaksysteem](https://gitlab.com/zaaksysteem) project.

## Gitlab-CI example
The image is actively used in the Gitlab.com-based CI/CD pipeline for building zaaksysteem application builds, but can be used as the base for any CI builder.

    # .gitlab-ci.yml
    image: registry.gitlab.com/zaaksysteem/zaaksysteem-builder:latest
    
    stages:
      - build
      - etc

## Copyright and License
Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
